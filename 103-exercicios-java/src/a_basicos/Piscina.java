package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe as três dimensões de uma piscina (largura,
 * comprimento, profundidade) e retorne a sua capacidade em litros.
 */
public class Piscina {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Digite o comprimento: ");
		double comprimento = scanner.nextFloat();

		System.out.println("Digite o largura: ");
		double largura = scanner.nextFloat();

		System.out.println("Digite o profundida: ");
		double profundidade = scanner.nextFloat();

		double volume = comprimento * largura * profundidade / 2;

		System.out.println(volume);
	}
}
