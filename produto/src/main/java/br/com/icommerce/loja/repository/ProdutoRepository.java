package br.com.icommerce.loja.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.icommerce.loja.model.Produto;

public interface ProdutoRepository extends CrudRepository <Produto, Integer> {

	Produto findById(int id);
}
