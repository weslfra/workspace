
public class TesteSistema {

	public static void main(String[] args) {
				
		
		Autenticavel referencia = new Gerente();
		referencia.setSenha(2222);

 		Autenticavel referencia2 = new Cliente();
 		referencia2.setSenha(2223);
 		
		Autenticavel referencia3 = new Administrador();
		referencia3.setSenha(2222);
		
		SistemaInterno si = new SistemaInterno();
		si.autentica(referencia);
		si.autentica(referencia2);
		si.autentica(referencia3);
	}

}
