package com.kazale.crud.api.documents;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Cliente {

	@Id
	private String id;
	private String nome;
	private String email;
	private String cpf;
	
	public Cliente() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

	
	public String getNome() {
		return nome;
	}

	@NotEmpty(message = "Nome nao pode ser vazio")
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	public String getEmail() {
		return email;
	}
	
	@Email(message = "Email invalido")
	@NotEmpty(message = "Email nao pode ser vazio")
	public void setEmail(String email) {
		this.email = email;
	}

	
	public String getCpf() {
		return cpf;
	}
	
	
	@NotEmpty(message = "CPF nao pode ser vazio")
	@CPF(message = "CPF invalido")
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
