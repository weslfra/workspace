package com.moinho.loja.repository;

import org.springframework.data.repository.CrudRepository;

import com.moinho.loja.model.Oferta;

public interface OfertaRepository  extends CrudRepository<Oferta, Integer>{
	
	//Oferta findByIdUsuario(int id_usuario);
	//Oferta findByIdAnuncio(int id_anuncio);
	//Oferta findByIds(int id_usuario, int id_anuncio);
	
}
