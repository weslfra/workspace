package com.moinho.loja.repository;


import org.springframework.data.repository.CrudRepository;

import com.moinho.loja.model.Anuncio;

public interface AnuncioRepository  extends CrudRepository<Anuncio, Integer>{
	
	Anuncio findById(int id);
	
}