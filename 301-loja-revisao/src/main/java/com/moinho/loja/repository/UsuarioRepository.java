package com.moinho.loja.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.moinho.loja.model.Usuario;

public interface UsuarioRepository  extends CrudRepository<Usuario, Integer>{
	
	Optional<Usuario> findByEmail(String email);
	
}
