package com.moinho.loja.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moinho.loja.model.Usuario;
import com.moinho.loja.repository.UsuarioRepository;
import com.moinho.loja.service.PasswordService;
import com.moinho.loja.service.TokenService;

@Controller
public class UsuarioController {
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	TokenService tokenService;
	
	@RequestMapping(path="/usuario", method=RequestMethod.POST)
	@ResponseBody
	public Usuario inserirUsuario(@RequestBody Usuario usuario) {
		String hash = passwordService.encode(usuario.getSenha());
		usuario.setSenha(hash);
		return usuarioRepository.save(usuario);
	}

	@RequestMapping(path="/usuarios", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Usuario> getVariosUsuarios() {		
		return usuarioRepository.findAll();
	}
	
	@RequestMapping(path="/usuario/{email}", method=RequestMethod.GET)
	@ResponseBody
	public Optional<Usuario> getUsuarioByEmail(@PathVariable String email) {		
		return usuarioRepository.findByEmail(email);
	}
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> logar(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBanco = usuarioRepository.findByEmail(usuario.getEmail());	
		
		if(! usuarioBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		boolean deuCerto = passwordService.verificar(usuario.getSenha(), usuarioBanco.get().getSenha());
		
		if(deuCerto) {
			String token = tokenService.gerar(usuarioBanco.get().getId());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			
			return new ResponseEntity<Usuario>(usuarioBanco.get(), headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();
	}
	
	@RequestMapping(path="/check", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> verificar(HttpServletRequest request) {
		String bearer = request.getHeader("Authorization");
		String token = bearer.replace("Bearer ", "");
		
		int idUsuario = tokenService.verificar(token);
		
		Optional<Usuario> usuarioBanco = usuarioRepository.findById(idUsuario);
		
		if(usuarioBanco.isPresent()) {
			return ResponseEntity.ok(usuarioBanco.get());
		}

		return ResponseEntity.badRequest().build();
	}
	
}

