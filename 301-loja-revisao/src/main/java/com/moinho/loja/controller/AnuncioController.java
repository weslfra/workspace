package com.moinho.loja.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moinho.loja.model.Anuncio;
import com.moinho.loja.model.Usuario;
import com.moinho.loja.repository.AnuncioRepository;
import com.moinho.loja.service.TokenService;

@Controller
public class AnuncioController {
	@Autowired
	AnuncioRepository anuncioRepository;
	
	@Autowired
	TokenService tokenService;
	
	@RequestMapping(path="/anuncio", method=RequestMethod.POST)
	@ResponseBody
	public Anuncio inserirAnuncio(HttpServletRequest request, @RequestBody Anuncio anuncio) {
		String bearer = request.getHeader("Authorization");
		String token = bearer.replace("Bearer ", "");
		
		int idUsuario = tokenService.verificar(token);
		
		Usuario usuario = new Usuario();
		usuario.setId(idUsuario);
		anuncio.setUsuario(usuario);
		anuncio.setStatus("ativo");
		
		return anuncioRepository.save(anuncio);
	}
	
	@RequestMapping(path="/anuncios", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Anuncio> getVariosAnuncios() {		
		return anuncioRepository.findAll();
	}
	
	@RequestMapping(path="/anuncio/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Anuncio getUsuarioByEmail(@PathVariable int id) {		
		return anuncioRepository.findById(id);
	}
}


