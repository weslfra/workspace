package com.moinho.loja.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.moinho.loja.model.Oferta;
import com.moinho.loja.repository.OfertaRepository;

@Controller
public class OfertaController {
	@Autowired
	OfertaRepository ofertaRepository;
	
	@RequestMapping(path="/oferta", method=RequestMethod.POST)
	@ResponseBody
	public Oferta inserirAnuncio(@RequestBody Oferta oferta) {
		return ofertaRepository.save(oferta);
	}
	
	@RequestMapping(path="/ofertas", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Oferta> getVariasOfertas() {		
		return ofertaRepository.findAll();
	}
	
//	@RequestMapping(path="/oferta/{id_anuncio}", method=RequestMethod.GET)
//	@ResponseBody
//	public Oferta getUsuarioByEmail(@PathVariable int id_anuncio) {		
//		return ofertaRepository.findByIdAnuncio(id_anuncio);
//	}
}


