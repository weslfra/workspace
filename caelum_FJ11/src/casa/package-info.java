/**
 * 
 */
/**
 * @author weslfra
 * 1) O objetivo dos exercícios a seguir é fixar os conceitos vistos. Se você está com dificuldade em alguma parte
desse capítulo, aproveite e treine tudo o que vimos até agora no pequeno programa abaixo:
		• Programa:
Classe: Casa Atributos: cor, totalDePortas, portas[] Métodos: void pinta(String s), int quantasPortasEs-
taoAbertas(), void adicionaPorta(Porta p), int totalDePortas()
Crie uma casa, pinte-a. Crie três portas e coloque-as na casa através do método adicionaPorta , abra
e feche-as como desejar. Utilize o método quantasPortasEstaoAbertas para imprimir o número de portas
abertas e o método totalDePortas para imprimir o total de portas em sua casa.
 */
package casa;