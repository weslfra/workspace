package casa;

public class Casa {

	String cor;
	static int totalDePortas = 0;
	Porta[] portas;

	public void pinta(String p) {
		this.cor = p;
	}

	public void adicionaPorta(Porta p) {

		boolean estacheio = true;

		for(int i = 0; i < portas.length; i++) {  
			if(portas[i] == null) { 
				estacheio = false;
				portas[i] = p;
				this.setTotalDePortas();
				break;
			} 
		}
	}

	public int getPortasEstaoAbertas() {

		int contadorDePortasAbertas = 0;


		for(int i = 0; i < this.portas.length; i++) {
			if(this.portas[i].aberta == true) {
				contadorDePortasAbertas += 1;
			} 
		}
		return contadorDePortasAbertas;
	}

	public int getPortasFechadas() {

		int contadorDePortasFechadas = 0;

		for (int i = 0; i < portas.length; i++) {

			if (portas[i].aberta == false) {

				contadorDePortasFechadas += 1;
			}
		}

		return contadorDePortasFechadas;
	}

	public String getCor() {
		return cor;
	}

	public void setTotalDePortas() {
		totalDePortas++;
	}

	public int getTotaldePortas() {
		return totalDePortas;
	}

}
