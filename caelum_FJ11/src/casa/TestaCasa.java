package casa;

import funcionario.Funcionario;

public class TestaCasa {
	public static void main(String[] args) {

		Casa casaTauany = new Casa();
		casaTauany.pinta("Rosa");
		
		casaTauany.portas = new Porta[5];
				
		for (int i = 0; i < 2; i ++) {
			Porta pBanheiros = new Porta();
			casaTauany.adicionaPorta(pBanheiros);
			pBanheiros.aberta = true;
		}
	
		Porta pTauany = new Porta();
		casaTauany.adicionaPorta(pTauany);
		pTauany.aberta = true;
		
		
		Porta pTaynah = new Porta();
		casaTauany.adicionaPorta(pTaynah);
		pTaynah.aberta = false;
		
		Porta pCidinha = new Porta();
		casaTauany.adicionaPorta(pCidinha);
		pCidinha.aberta = false;
		
		System.out.println("---------------------------");		
		System.out.println("A cor da casa da Tayuany - " + casaTauany.getCor());
		System.out.println("Numero de portas - " + casaTauany.getTotaldePortas());
		System.out.println("Portas abertas - " + casaTauany.getPortasEstaoAbertas());
		System.out.println("Portas fechas - " + casaTauany.getPortasFechadas());
		
				
		
		
		
	}
}