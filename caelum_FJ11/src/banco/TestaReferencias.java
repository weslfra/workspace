package banco;

public class TestaReferencias {
	
		public static void main(String[] args) {
			
			Conta c1 = new Conta();
			c1.deposita(100);
			
			Conta c2 = new Conta();
			c2.deposita(200);
			
			System.out.println(c1.saldo);
			System.out.println(c2.saldo);
			
			Conta c3 = new Conta();
			c3.deposita(500);
			
			Conta c4 = new Conta();
			c4 = c3;
			
			System.out.println(c3.saldo);
			System.out.println(c4.saldo);
			
		}

}
