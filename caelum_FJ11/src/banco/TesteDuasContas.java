package banco;

public class TesteDuasContas {
	
	public static void main(String[] args) {
		
		Conta minhaConta = new Conta();
		minhaConta.saldo = 1000;
		
		Conta meuSonho = new Conta();
		meuSonho.saldo = 1500000;
		
		System.out.println("Saldo minhaConta: " + minhaConta.saldo);
		System.out.println("Saldo meuSonho: " + meuSonho.saldo);
		
		minhaConta.transfere(meuSonho, 1000);
		
		System.out.println("Saldo atual minhaConta: " + minhaConta.saldo);
		System.out.println("Saldo atual meuSonho: " + meuSonho.saldo);
				
		
	}

}
