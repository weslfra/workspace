package banco;

public class Programa {

	public static void main(String[] args) {
		
//		Criar a conta
		Conta minhaConta;		
		minhaConta  = new Conta();
		
//		Alterando os valores da Minha Conta
		minhaConta.dono = "Duke";
		minhaConta.saldo = 1000.0;
		
//		Sacar 200 reias
		
		if (minhaConta.saca(200)) {
			System.out.println("Conseguiu Sacar");
		}
		else {
			System.out.println("Saldo insuficiente");
		}
			
	
//		Depositar 50 reais
		minhaConta.deposita(50);

		System.out.println("Saldo Atual: " + minhaConta.saldo);
		
	
	}
}
