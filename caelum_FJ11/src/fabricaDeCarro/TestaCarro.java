package fabricaDeCarro;

public class TestaCarro {

	public static void main(String[] args) {
		
		Carro meuCarro = new Carro();
		
		meuCarro.cor = "Verde";
		meuCarro.modelo = "Fusca";
		meuCarro.velocidadeAtual = 0;
		meuCarro.velocidadeMaxima = 80;
		
//		ligar o carro
		meuCarro.liga();
		
		
//		acelera o carro
		meuCarro.acelera(40);
		System.out.println(meuCarro.velocidadeAtual);
		
		
		Motor motorDoMeuCarro = new Motor();
		meuCarro.motor  = motorDoMeuCarro;
		meuCarro.motor.tipo = "FODA";
		
		
		System.out.println(meuCarro.motor.tipo);
		
		
		
	
	}
}
