package fabricaDeCarro;

public class Carro {

		String modelo;
		String cor;
		double velocidadeAtual;
		double velocidadeMaxima;
		Motor motor;
		
		void liga() {
			
			System.out.println("O carro esta ligado!");
			
		}
		
		void acelera(double valor) {
			
			this.velocidadeAtual += valor;
			
		}
		
		int pegaMarcha() {
			
			if (this.velocidadeAtual < 0) {
				return -1;
			}
			if (this.velocidadeAtual >= 0 && this.velocidadeMaxima < 40) {
				return 1;
			}
			if (this.velocidadeAtual >= 40 && this.velocidadeMaxima < 80 ) {
				return 2;
			}
			return 3;
		}
		
		
		
}
