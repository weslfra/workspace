package bilhete;

public class TesteArray {

		public static void main(String[] args) {
			
			Conta contaNova = new Conta();
			contaNova.saldo = 1000.0;
			
			Conta[] minhasContas = new Conta[10];
			
			minhasContas[0] = contaNova;
					
			minhasContas[1] = new Conta();
			minhasContas[1].saldo = 3200.0;
			
			System.out.println(minhasContas[0].getSaldo());
			System.out.println(minhasContas[1].getSaldo());
			
		
			
		}
}
