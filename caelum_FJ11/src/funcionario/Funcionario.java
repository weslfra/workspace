package funcionario;

public class Funcionario {

		String nome;
		String departamento;
		double salario;
		Data dataDeEntrada;
		String rg;
		boolean status;
		
		
		void bonifica(double aumento){
			
			this.salario += aumento;
			
		}
		
		void demite () {
			
			this.status = false;
			
		}
		
		
		
		void monstra() {
			
			System.out.println("Nome: " + this.nome);
			System.out.println("Departamento: " + this.departamento);
			System.out.println("Salario: " + this.salario);
			System.out.println("Data de Entrada: " + this.dataDeEntrada.dia + "/" + this.dataDeEntrada.mes + "/" + this.dataDeEntrada.ano);
			System.out.println("RG: " + this.rg);
			System.out.println("Satus: " + this.status);
			System.out.println("-----------------------------------------");
						
		}
		
		
}
