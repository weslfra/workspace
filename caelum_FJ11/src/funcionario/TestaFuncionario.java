package funcionario;

public class TestaFuncionario {

	public static void main(String[] args) {
		
		Funcionario f1 = new Funcionario();
		
		f1.nome = "Chico";
		f1.departamento = "T.I";
		f1.rg = "47.086.729-2";
		f1.salario = 100;
		f1.status = true;
		
		Data data = new Data();
		f1.dataDeEntrada = data;
		f1.dataDeEntrada.dia = 17;
		f1.dataDeEntrada.mes = 9;
		f1.dataDeEntrada.ano = 1990;
		
		f1.monstra();
				
		Funcionario f2 = new Funcionario();
		f2.dataDeEntrada = data;
  		f2.nome = "Nicolas";
		f2.salario = 200;
		f2.monstra();
			
		
		if (f1 == f2) {
			System.out.println("Funcionarios sao iguais");
		}
		else {
			System.out.println("Funcionarios diferentes");
		}
		
		
		
	}
}
